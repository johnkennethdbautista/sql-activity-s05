-- 1
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5
SELECT customerName FROM customers WHERE state IS NULL;

-- 6
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10
SELECT DISTINCT country FROM customers;

-- 11
SELECT DISTINCT status FROM orders;

-- 12
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "FRANCE" OR country = "CANADA";

-- 13
SELECT employees.firstName, employees.lastName, offices.city FROM employees
JOIN offices ON offices.officeCode = employees.officeCode
WHERE offices.city = "TOKYO";

-- 14
SELECT customers.customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

-- 15
SELECT products.productName, customers.customerName FROM orderdetails
JOIN products ON products.productCode = orderdetails.productCode
JOIN orders ON orders.orderNumber = orderdetails.orderNumber
JOIN customers ON customers.customerNumber = orders.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

-- 16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE offices.country = customers.country;

-- 17
SELECT products.productName, products.quantityInStock FROM products
JOIN productlines ON productlines.productLine = products.productLine
WHERE products.productLine = "Planes" AND products.quantityInStock < 1000;

-- 18
SELECT customerName FROM customers
WHERE phone LIKE "%+81%";